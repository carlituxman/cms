# -*- coding: utf-8 -*-
"""
        Ajustes de producción
"""

DEBUG = True
TEMPLATE_DEBUG = DEBUG
PREPEND_WWW = False #not DEBUG
SEND_BROKEN_LINK_EMAILS = False #not DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'cms_demo',                      # Or path to database file if using sqlite3.
        'USER': 'cms_demo',                      # Not used with sqlite3.
        'PASSWORD': 'Django',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

ROOT_URLCONF = 'myproject.urls'

MEDIA_ROOT = '/home/informatik/webapps/cms_demo_media'
STATIC_ROOT = '/home/informatik/webapps/cms_demo_static'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

EMAIL_SUBJECT_PREFIX= '[CMS_DEMO] '
EMAIL_HOST='smtp.webfaction.com'
EMAIL_HOST_USER='informatik_log'
EMAIL_HOST_PASSWORD='seron268'
EMAIL_PORT='25'
DEFAULT_FROM_EMAIL = "log@seronalia.com"
SERVER_EMAIL = "log@seronalia.com"

WSGI_APPLICATION = 'myproject.wsgi.application'
