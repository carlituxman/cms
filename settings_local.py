# Django settings for project.

import os




PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
BASE_DIR = os.path.realpath(os.path.dirname(__file__))

DEBUG = True
TEMPLATE_DEBUG = True
PREPEND_WWW = False
SEND_BROKEN_LINK_EMAILS = False

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'cms1',                      # Or path to database file if using sqlite3.
        'USER': 'root',                      # Not used with sqlite3.
        'PASSWORD': 'root',                  # Not used with sqlite3.
        'HOST': '127.0.0.1',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(PROJECT_PATH, 'database.sqlite'),
#     }
# }

SITE_ROOT = os.path.join(PROJECT_ROOT, 'site')
MEDIA_ROOT = os.path.join(SITE_ROOT, 'media')
STATIC_ROOT = os.path.join(SITE_ROOT, 'static')

#CACHES = {
    #'default': {
        #'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    #}
#}

MIDDLEWARE_CLASSES = (
    # 'localeurl.middleware.LocaleURLMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.redirects.middleware.RedirectFallbackMiddleware',
    'middleware.ip_proxy.SetRemoteAddrFromForwardedFor',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(PROJECT_ROOT, 'whoosh_index'),
    },
}

INTERNAL_IPS = ('127.0.0.1',)

INSTALLED_APPS = (
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    # 'django_admin_bootstrapped',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.redirects',
    'django.contrib.sitemaps',
    'django.contrib.localflavor',
    'mptt',
    'south',
    'sorl.thumbnail',
    'debug_toolbar',
    # 'haystack',   #./manage.py rebuild_index / update_index (cron)
    'banners',
    'catalog',
    'stats',
    'configuracion',
    'contact',
    'metatags',
    'quienes',
    'slider',
    'social',
    'testimonios',
    'web',
    'clientes',
    'proyectos',
    'newsletter',
    'feincms',
    # 'localeurl',
    # 'transmeta',    #./manage.py sync_transmeta_db (ejemplo en Slider)
)

# import re
# LOCALE_INDEPENDENT_PATHS = (
#     re.compile('^/aviso-legal/'),
#     re.compile('^/privacidad/'),
# )
# REDIRECT_LOCALE_INDEPENDENT_PATHS = False
# PREFIX_DEFAULT_LANGUAGE = True


# to launch fake mail server in localhost run:
# python -m smtpd -n -c DebuggingServer localhost:1025

EMAIL_SUBJECT_PREFIX='[LOCAL] '
EMAIL_HOST='localhost'
EMAIL_PORT='1025'

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False
}