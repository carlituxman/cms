# -*- coding: utf-8 -*-
# coding=UTF-8
'''
    menudia - models : platos y centros
'''

from django.db import models


class Plato(models.Model):
    """
    Plato
    """
    GENDER_CHOICES = (
        ('1', 'Primer plato'),
        ('2', 'Segundo plato'),
        ('3', 'Postre'),
    )

    nombre      = models.CharField(verbose_name=_(u'nombre'), max_length=120, help_text=_(u'Nombre del plato'))

    
    def __unicode__(self):
        return self.nombre
    
    class Meta:
        verbose_name = _(u'Plato')
        verbose_name_plural = _(u'Platos')
        ordering = ['nombre',]


class Centro(models.Model):
    """
    Menú del Día
    """
    nombre      = models.CharField(verbose_name=_(u'título'), max_length=120, help_text=_(u'Título'))
    
    
    def __unicode__(self):
        return self.nombre
    
    class Meta:
        verbose_name = _(u'Centro')
        verbose_name_plural = _(u'Centros')
        ordering = ['nombre',]



class Provincia(models.Model):

    nombre      = models.CharField(verbose_name=_(u'provincia'), max_length=120, help_text=_(u'Provincia'))



class Poblacion(models.Model):

    nombre      = models.CharField(verbose_name=_(u'poblacion'), max_length=120, help_text=_(u'Población'))