# haystack search_indexes.py

import datetime
from haystack import indexes
from web.models import Pagina

class PaginaIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    # author = indexes.DateTimeField(model_attr='nombre')
    # pub_date = indexes.DateTimeField(model_attr='creado_el')

    def get_model(self):
        return Pagina

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(creado_el__lte=datetime.datetime.now())
