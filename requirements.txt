#django<1.5
south
sorl-thumbnail
django-debug-toolbar
django-mptt
docutils
feincms==1.5.3
#PIP en local para desarrollo
django-compressor
cssmin
slimit
mysql-python
django-admin-tools

#-MULTILENGUAJE-
#django-localeurl
#django-transmeta

#-BUSCAR-
#django-haystack > pip install -e git+https://github.com/toastdriven/django-haystack.git@master#egg=django-haystack
#whoosh